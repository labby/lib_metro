<?php

/**
 *  @module      	Metro UI
 *  @version        see info.php of this module
 *  @author         cms-lab, metro ui by Sergey Pimenov
 *  @copyright      2018-2022 CMS-LAB
 *  @license        http://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory = 'lib_metro';
$module_name      = 'Metro Library';
$module_function  = 'library';
$module_version   = '4.5.0.0';
$module_platform  = '5.x';
$module_delete	  =  true;
$module_author    = 'cms-lab';
$module_license   = 'https://opensource.org/licenses/MIT';
$module_license_terms   = '-';
$module_description = '<a href="https://metroui.org.ua" target="_blank">Metro UI</a>: Metro 4 is an open source toolkit for developing with HTML, CSS, and JS.';
$module_guid      = 'e520cf74-182b-4b9f-bc90-a4c5c56a1760';
$module_home      = 'https://www.cms-lab.com';
